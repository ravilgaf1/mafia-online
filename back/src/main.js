import express from 'express';
import { Mongo } from './class/Mongo.js';
import { Room } from './class/Room.js';
import { User } from './class/User.js';
import { Server as SocketIOServer } from 'socket.io';
import { createServer } from 'http';
import cors from "cors";
import bodyParser from "body-parser";

const app = express();
app.use(cors({
	origin: "*",
	methods: "GET,PUT,POST"
}));
app.use(bodyParser.json())

const server = createServer(app);
const io = new SocketIOServer(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST"],
	}
});
const PORT = 3000;
const mongo = new Mongo();

// Подключение к MongoDB при старте сервера
const connectDB = async () => {
	try {
		console.log("Connecting to MongoDB...");
		await mongo.connect();
		console.log("MongoDB connected!");
	} catch (error) {
		console.error("Could not connect to MongoDB:", error);
		process.exit(1); // Закрыть сервер, если подключение не удалось
	}
};


// Получение данных о комнате
app.get("/room/:id", async (req, res) => {
	try {
		const room = await new Room(mongo).getData(req.params.id);
		res.json(room);
	} catch (error) {
		throw error;
	}
})

// Получение данных о пользователе
app.get("/user/:id", async (req, res) => {
	try {
		const user = await new User(mongo).getData(req.params.id);
		res.json(user);
	} catch (error) {
		throw error;
	}
})

// Добавление нового пользовтателя
app.post("/user", async (req, res) => {
	try {
		const user = await new User(mongo).create();
		res.json(user);
	} catch (error) {
		throw error;
	}
})

// Указание имени пользователя
app.put("/user/name/:id", async (req, res) => {
	const user_id = req.params.id;
	const name = req.body.name;

	const user = await new User(mongo).giveName(user_id, name);

	res.json(user);
})

io.on("connection", (socket) => {
	console.log("Client connected:", socket.id);

	socket.on("createRoom", async (data) => {
		try {
			const { numPeaceful, numMafia, numDoctor, numDetectives } = data;

			const roomConfig = {
				numPeaceful,
				numMafia,
				numDoctor,
				numDetectives,
			};

			const rooms = new Room(mongo);
			const result = await rooms.create(roomConfig);
			socket.emit("roomCreated", result);
		} catch (error) {
			socket.emit("error", "Error creating room", error);
		}
	})

	socket.on("joinRoom", async ({ user_id, room_id }) => {
		socket.join(room_id);

		const user = await new User(mongo).getData(user_id); // Получаем пользователя по ID
		const room = await new Room(mongo).getData(room_id);// Получаем комнату по ID

		if (!room?.users.find((item) => item["_id"] === user["_id"])) {
			// Добавление пользователя в rooms.users
			await mongo.getCollection("rooms").updateOne(
				{ _id: room_id },
				{
					$push: {
						users: {
							$each: [{ "_id": user["_id"], role: null, isAdmin: room.users.length === 0 }]
								}
					}
				}
			)
		}

		socket.to(room_id).emit("newUserJoined", { user, room_id });
		console.log(`User ${user_id} joined room ${room_id}`);
	})

	socket.on("leaveRoom", async (roomId) => {
		socket.leave(roomId);
		socket.to(roomId).emit("userLeft", { userId: socket.id, roomId });
		console.log(`User ${socket.id} left room ${roomId}`);
	})

	socket.on("disconnect", () => {
		console.log(`Client disconnected: ${socket.id}`);
	})

	// Смена имени пользователя
	socket.on("changeName", async ({ user_id, name, room_id }) => {
		await new User(mongo).giveName(user_id, name);

		if (room_id) {
			io.in(room_id).emit("getChangeName", { _id: user_id, name, test: "test" });
		}	
	})

	// Получение пользователей в комнате
	socket.on("roomUsers", async (room_id) => {
		const room = await new Room(mongo).getData(room_id);
		if (room && room.users) {
			// Получаем массив ID пользователей
			const userIds = room.users.map(user => user._id);

			// Запрос в коллекцию пользователей с использованием оператора $in для поиска всех пользователей с ID из userIds
			const users = await mongo.getCollection("users").find({
				_id: { $in: userIds }
			}).toArray();

			console.log({users, userIds});


			socket.emit("getRoomUsers", users.map((user, index) => ({ ...user, ...room.users[index] })))
		} else {
			console.log("No room found or no users in room");
		}
	});

	// Смена статуса комнаты. Например если все пользователи выбрали себе карту, меняется статус комнаты на игру.
	socket.on("changeStatusRoom", async ({room_id, status}) => {
		await mongo.getCollection("rooms").updateOne(
			{ _id: room_id },
			{ $set: {status: status} },
		);

		io.in(room_id).emit("getChangeStatusRoom", status);
	});

	// TakeCards выбор карты из вариантов и присвоение статуса пользователю
	socket.on("selectRoleCard", async ({ room_id, user_id, cards }) => {
		// Попробуем получить данные о комнате
		let room;
		try {
			room = await new Room(mongo).getData(room_id);
		} catch (error) {
			console.error("Error fetching room data:", error);
			return;
		}
	
		// Проверяем, что комната действительно получена
		if (!room) {
			console.log("Room not found:", room_id);
			socket.emit('error', 'Room not found');
			return;
		}
	
		if (!room.users || room.users.length === 0) {
			console.log("No users in room:", room_id);
			socket.emit('error', 'No users in room');
			return;
		}
	
		// Выбираем случайную карту роли
		const randomCardIndex = Math.floor(Math.random() * cards.length);
		const newRole = cards[randomCardIndex];
	
		// Проверяем наличие пользователя в комнате
		const user = room.users.find(u => u._id.toString() === user_id);
		if (!user) {
			console.log("User not found in room:", user_id);
			socket.emit('error', 'User not found in room');
			return;
		}
	
		// Обновляем роль пользователя в комнате
		try {
			const updateResult = await mongo.getCollection("rooms").updateOne(
				{ _id: room_id, "users._id": user_id },
				{ $set: { "users.$.role": newRole } }
			);
	
			if (updateResult.modifiedCount === 0) {
				console.log("No documents were updated");
				socket.emit('error', 'Update failed');
				return;
			}
	
			// Оповещаем пользователя о новой роли
			io.to(room_id).emit('roleUpdated', randomCardIndex);
			console.log("Role updated successfully:", newRole);
		} catch (updateError) {
			console.error("Failed to update role:", updateError);
			socket.emit('error', 'Failed to update role');
		}
	});
})

server.listen(PORT, async () => {
	await connectDB();
	console.log(`Server listening on port: ${PORT}`);
})

// Отключение от MongoDB при завершении процесса сервера
process.on('SIGINT', async () => {
	console.log("Disconnecting MongoDB...");
	await mongo.disconnect();
	console.log("MongoDB disconnected. Server shutting down.");
	process.exit(0);
});
