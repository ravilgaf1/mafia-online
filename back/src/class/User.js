import { v4 as uuidv4 } from 'uuid';

export class User {
	constructor(mongo, name) {
		this.id = uuidv4();
		this.mongo = mongo;
		this.name = name || "";
	}

	// Создание пользователя
	async create() {
		return await this.mongo.getCollection("users").insertOne({ _id: this.id, name: this.name });
	}

	// Получения всех данных о пользователе из базы данных
	async getData(id) {
		return this.mongo.getCollection("users").findOne({ _id: id });
	}

	// Указывает имя для пользователя
	async giveName(user_id, name) {
		const user = this.mongo.getCollection("users").updateOne(
			{ _id: user_id },
			{ $set: { name: name } }
		);

		return user;
	}
}