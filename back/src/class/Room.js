import { v4 as uuidv4 } from 'uuid';

export class Room {
	constructor(mongo, users = []) {
		this.id = uuidv4();
		this.users = users;
		this.mongo = mongo;
	}

	async create(config) {
		const room = await this.mongo.getCollection("rooms").insertOne({ _id: this.id, users: [], config: config, status: "wait_user" });
		return room;
	}

	async getData(id) {
		const room = await this.mongo.getCollection("rooms").findOne({ _id: id });
		return room;
	}
}

/**
 * Room:
 * - id (uuid)
 * - users - array
 */