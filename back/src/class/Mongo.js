import { MongoClient } from "mongodb";
import "dotenv/config"

export class Mongo {
	constructor() {
		this.client = new MongoClient(process.env.MONGO_URL)
		this.db = null;
	}

	async connect() {
		try {
			await this.client.connect();
			console.log("Connected successfully to MongoDB server");
			this.db = this.client.db(process.env.MONGO_DB_NAME);
		} catch (error) {
			console.error("Failed to connect to MongoDB", error)
		}
	}

	getCollection(name) {
		if (!this.db) {
			throw new Error("Not connected to database");
		}

		return this.db.collection(name);
	}

	async disconnect() {
		await this.client.close();
		console.log("Disconnected from MongoDB");
	}
}