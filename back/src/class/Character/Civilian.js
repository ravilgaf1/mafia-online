import { Character } from "./Character.js";

export class Civilian extends Character {
	constructor(name) {
		super(name, "Мирный");
	}

	act() {
		console.log(`${this.name} (${this.type}): спит и ничего не подозревает.`);
	}
}