import { Character } from "./Character.js";

export class Detective extends Character {
	constructor(name) {
		super(name, "Комиссар");
	}

	act() {
		console.log(`${this.name} (${this.type}): пытается раскрыть мафию.`);
	}
}