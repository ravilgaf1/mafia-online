import { Character } from "./Character.js";

export class Mafia extends Character {
	constructor(name) {
		super(name, "Мафия");
	}

	act() {
		console.log(`${this.name} (${this.type}): выбирает цель для убийства.`);
	}
}