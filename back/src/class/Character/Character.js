export class Character {
	constructor(type, name) {
		this.type = type;
		this.name = name;
	}

	act() {
		console.log(`${this.name} (${this.type}): совершает действие.`);
	}	
}