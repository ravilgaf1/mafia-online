import { Character } from "./Character.js";

export class Doctor extends Character {
	constructor(name) {
		super(name, "Врач");
	}

	act() {
		console.log(`${this.name} (${this.type}): выбирает кого спасти этой ночью.`);
	}
}