import { browser } from "$app/environment"
import { PUBLIC_SERVER_URL } from "$env/static/public";

export const load = async () => {
	try {
		if (browser) {
			const user_id = window.localStorage.getItem("user_id");

			if (user_id) {
				const response = await fetch(PUBLIC_SERVER_URL + `user/${user_id}`);
	
				if (!response.ok) {
					throw "Can't get data of user";
				}
	
				const result = await response.json();
				return {result};
			} else {
				const response = await fetch(PUBLIC_SERVER_URL + `user`, { method: "POST" });

				if (!response.ok) {
					throw "Can't create user";
				}

				const result = await response.json();

				window.localStorage.setItem("user_id", result.insertedId)
				return {result};
			}
		}
	} catch (error) {
		throw error;
	}
	
}